# Project Setup
## Introduction
This section provides the steps to set up a running version of
this project on one's personal computer. It show how you can run
the project without docker and with docker.

## Project dependencies
First install all dependencies of the project using composer.

```bash
composer install
```

If any errors occur fix them and try again to install the
dependencies. For example, I got the error:

```bash
- jakoch/phantomjs-installer 2.1.1-p08 requires ext-bz2 * -> the requested PHP extension bz2 is missing from your system.                    
```
And to fix it I had to uncomment the line with the required
extension in my '/etc/php/php.ini' file.

## Environment Variables
Move the .env.example file to .env and change the configurations
so as it works with your system.

```bash
cp .env.example .env
```

The following shows the changes I made to the default .env file.
Take note that you only choose one of the API_DOMAIN in your
config:

```bash
# For running in docker
API_DOMAIN=172.17.0.4
# For running without docker
API_DOMAIN=http://localhost
API_DEBUG=true

DB_CONNECTION=mysql
DB_HOST=172.17.0.3
DB_PORT=3306
DB_DATABASE=chanjo
DB_USERNAME=root
DB_PASSWORD=password
```

If using docker, with my configuration, I'll access the api when I
vist the url 172.17.0.4. This means that docker should set up the
ip for this image as the same 172.17.0.4. Docker assigns ips from
172.17.0.2 and so forth. So if the this will be the first image
you use, then change the ip to 172.17.0.2. You can get the ip of
an image by running:

```bash
docker inspect image-name
```

## Running Without Docker
Run the following command in the root of the chanjo-api project:

```bash
php -S localhost:7000 -t public
```

This will serve the project form the public folder. To confirm
this works you can run the following command from the terminal:
```bash
curl --header "Content-Type: application/json" --request POST --data '{"email":"jirrow@gmail.com", "password":"password"}' http://localhost:7000/api/login
```
A long string token should be returned if the setup was
successful.

## Running With Docker
Assuming you already have docker set up and running, while in the
root of the project run the following command:

```bash
docker build -t chanjo-api .
```

To run the project do:

```bash
docker run chanjo-api
```

And visit the ip using the browser. For example if you visit
(assuming your docker image has the same ip): 172.17.0.4/users you
should get a 'Token not provided' error.

To confirm that everything runs as expected, running the following
command should return a token:

```bash
curl --header "Content-Type: application/json" --request POST --data '{"email":"jirrow@gmail.com", "password":"password"}' 172.17.0.4/api/login
```

# Helper Commands
To run the get all the necessary api calls from the project use:

```bash
php artisan route:list
```

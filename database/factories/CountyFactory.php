<?php

use Faker\Generator as Faker;

$factory->define(App\County::class, function (Faker $faker) {
    return [
        'county_name' => $faker->name,
        'region_id' => function () {
            return factory(App\Region::class)->create()->id;
        }
    ];
});

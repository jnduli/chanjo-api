<?php

use Faker\Generator as Faker;

$factory->define(App\Facility::class, function (Faker $faker) {
    return [
        'mfl' => 0,
        'facility_name' => $faker->name,
        'subcounty_id' => function () {
            return factory(App\Subcounty::class)->create()->id;
        },
        'county_id' => function() {
            return factory(App\County::class)->create()->id;
        },
        'region_id' => function() {
            return factory(App\Region::class)->create()->id;
        }
    ];
}); 

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'transaction_type';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'type'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

  public function transactions()
    {
        return $this->hasMany('App\Transaction','type');
    }
}

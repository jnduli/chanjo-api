<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreFridge extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'store_fridges';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'fridge_id', 'equipment_id', 'store_id', 'temperature_monitor_no', 'power_source', 'fridge_status',
      'fridge_alias' ,'installation_year' ,'delete_status'
  ];


  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

  public function store()
  {

      return $this->belongsTo('App\Store');

  }

  public function fridge()
  {

      return $this->hasOne('App\Fridge');

  }

}

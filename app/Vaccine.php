<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'vaccines';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'vaccine_name', 'doses_required', 'wastage_factor','vaccine_formulation','mode_administaration',
      'vaccine_presentation','vaccine_pck_vol','diluents_pck_vol','vaccine_price_vial','vaccine_price_dose',
      'alias','product_type','vvm_type','manufacturer','origin','active'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

  public function transaction_item()
    {
        return $this->hasMany('App\Transactionitem');
    }

    public function batch_balance()
    {

        return $this->hasMany('App\BatchBalance');

    }
}

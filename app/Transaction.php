<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'transactions';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'transaction_date', 'transaction_voucher', 'location_id','level','to_from',
      'user_id','type','status','type','request_id'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

  /**
     * Get the transactiontype that owns the transaction.
     */

  public function transaction_type()
    {
        return $this->belongsTo('App\TransactionType','type');
    }

  public function transaction_items()
    {
        return $this->hasMany('App\TransactionItem');
    }

  public function vaccines()
    {
        return $this->belongsTo('App\Vaccine');
    }

  public function users()
    {
        return $this->belongsTo('App\User');
    }

  public function locations()
    {
        return $this->belongsTo('App\Location');
    }






}

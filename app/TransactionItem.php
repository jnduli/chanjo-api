<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'transaction_items';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'vaccine_id', 'batch', 'expiry_date','vvm','transaction_quantity',
      'max_quantity','min_quantity','current_quantity','comment','transaction_id'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

  /**
     * Get the transaction that owns the transaction_item.
     */

  public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
    
  public function vaccines()
    {
        return $this->belongsTo('App\Vaccine');
    }

}

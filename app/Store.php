<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'store';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'name', 'location_id', 'catchment_population', 'live_birth_population', 'pop_pregnant_women', 'pop_surviving_infants',
      'pop_adolescent_girls' ,'cold_boxes' ,'vaccine_carriers','ice_packs', 'time_to_store', 'distance_to_store', 'nearest_town',
      'electricity_status', 'roof_type', 'manager_name', 'manager_phone', 'road_access', 'road_quality', 'special_access', 'road_quality',
      'special_access' ,'active' ,'created_at', 'update_at'
  ];



  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

  public function store_fridges()
  {

      return $this->hasMany('App\StoreFridge');

  }

  public function location()
  {

      return $this->belongsTo('App\Location');

  }

}

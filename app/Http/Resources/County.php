<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class County extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'county_name' => $this->county_name,
          'region_id' => $this->region_id,

      ];
    }
}

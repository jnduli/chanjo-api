<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Store as StoreResource;

class StoreFridge extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'fridge' => $this->fridge_id,
          'equipment_id' => $this->equipment_id,
          'store' => new StoreResource($this->store),
          'power_source' => $this->power_source,
          'fridge_status' => $this->fridge_status,
          'fridge_alias' => $this->fridge_alias,
          'installation_year' => $this->installation_year

      ];

    }
}

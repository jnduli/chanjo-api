<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Subcounty extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'subcounty_name' => $this->subcounty_name,
          'region_id' => $this->region_id,
          'county_id' => $this->county_id,

      ];
    }
}

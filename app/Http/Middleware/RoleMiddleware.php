<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (! $request->user()->is($role)) {
            return response()
                ->json([
                    'success' => false,
                    'status' => 403,
                    'message' => 'HTTP_FORBIDDEN'
                ], 403);
        }

        return $next($request);
    }
}

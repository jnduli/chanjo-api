<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Facility;
use Response;
use Validator;
use Illuminate\Validation\Rule;
use App\Http\Resources\Facility as FacilityResource;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = Facility::paginate();
        return FacilityResource::collection($facilities);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
        'organisationunitid' => 'required|unique:facilities',
        'facility_name' => 'required|max:255',
        'mfl' => 'required|unique:facilities',
        'total_population' => 'required|numeric',
        'under_one_population' => 'required',
        'women_population' => 'required',
        'catchment_population' => 'required',
        'live_birth_population' => 'required',
        'nearest_town' => 'required|max:255',
        'nearest_town_distance' => 'required',
        'nearest_store_distance' => 'required',
        'cold_boxes' => 'required',
        'vaccine_carriers' => 'required',
        'ice_packs' => 'required',
        'immunizing_status' => 'required|max:5',
        'operation_status' => 'required|max:20',
        'owner' => 'required|max:20',

    ]);

        if ($validator->fails()) {
            return Response::json([
                'error' => [
                    'message' => $validator->errors()->all(),

                ]
            ], 422);

        } else {

          //add facility

          $facility->facility_name = $request->input('facility_name');
          // $facility->mfl = $request->input('mfl');
          $facility->organisationunitid = $request->input('organisationunitid');
          $facility->total_population = $request->input('total_population');
          // $facility->under_one_population = $request->input('under_one_population');
          // $facility->women_population = $request->input('women_population');
          // $facility->catchment_population = $request->input('catchment_population');
          // $facility->live_birth_population = $request->input('live_birth_population');
          // $facility->nearest_town = $request->input('nearest_town');
          // $facility->nearest_town_distance = $request->input('nearest_town_distance');
          // $facility->nearest_store_distance = $request->input('nearest_store_distance');
          // $facility->cold_boxes = $request->input('cold_boxes');
          // $facility->vaccine_carriers = $request->input('vaccine_carriers');
          // $facility->ice_packs = $request->input('ice_packs');
          // $facility->immunizing_status = $request->input('immunizing_status');
          // $facility->operation_status = $request->input('operation_status');
          // $facility->owner = $request->input('owner');

          // $facility->save();

          //add store

          // $store = new Store;
          // $store->attr = $request->input('attr');
          // $store->save();


            return Response::json([
                'payload' => [
                    'message' => $request->input('facility_name').' was Added.',

                ]
            ], 200);


        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $facility = Facility::findOrFail($id);
      return new FacilityResource($facility);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $facility = Facility::findOrFail($id);
      //validate
      $validator = Validator::make($request->all(), [
        'organisationunitid' => [
        'required',
        Rule::unique('facilities')->ignore($facility->id),
    ],
      'facility_name' => 'required|max:255',
      // 'mfl' => 'required|unique:facilities',
      'total_population' => 'required|numeric',
      // 'under_one_population' => 'required',
      // 'women_population' => 'required',
      // 'catchment_population' => 'required',
      // 'live_birth_population' => 'required',
      // 'nearest_town' => 'required|max:255',
      // 'nearest_town_distance' => 'required',
      // 'nearest_store_distance' => 'required',
      // 'cold_boxes' => 'required',
      // 'vaccine_carriers' => 'required',
      // 'ice_packs' => 'required',
      // 'immunizing_status' => 'required|max:5',
      // 'operation_status' => 'required|max:20',
      // 'owner' => 'required|max:20',

  ]);

      if ($validator->fails()) {
          return Response::json([
              'error' => [
                  'message' => $validator->errors()->all(),

              ]
          ], 422);

      } else {

        //update facility

          $facility->facility_name = $request->input('facility_name');
          // $facility->mfl = $request->input('mfl');
          $facility->organisationunitid = $request->input('organisationunitid');
          $facility->total_population = $request->input('total_population');
          // $facility->under_one_population = $request->input('under_one_population');
          // $facility->women_population = $request->input('women_population');
          // $facility->catchment_population = $request->input('catchment_population');
          // $facility->live_birth_population = $request->input('live_birth_population');
          // $facility->nearest_town = $request->input('nearest_town');
          // $facility->nearest_town_distance = $request->input('nearest_town_distance');
          // $facility->nearest_store_distance = $request->input('nearest_store_distance');
          // $facility->cold_boxes = $request->input('cold_boxes');
          // $facility->vaccine_carriers = $request->input('vaccine_carriers');
          // $facility->ice_packs = $request->input('ice_packs');
          // $facility->immunizing_status = $request->input('immunizing_status');
          // $facility->operation_status = $request->input('operation_status');
          // $facility->owner = $request->input('owner');

          $facility->save();

        //add store

        // $store = new Store;
        // $store->attr = $request->input('attr');
        // $store->save();


          return Response::json([
              'payload' => [
                  'message' => $request->input('facility_name').' was Edited.',

              ]
          ], 200);


      }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

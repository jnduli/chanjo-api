<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;
// use Laravel\Lumen\Testing\DatabaseTransactions;

class ApiEndPointsTest extends TestCase
{
    // use DatabaseMigrations;
    use DatabaseTransactions;

    public function testSignInEndPointFailure()
    {
        $response = $this->post('/api/login', [
            'email'=> 'Samuel@ssfd.com',
            'password'=> 'shouldntwork'
        ]);
        $response->assertStatus(401);
    }

    public function testLogout()
    {
        $user = factory('App\User')->create();
        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();

        $response = $this->actingAs($user)
             ->get('/api/logout', ['Authorization' => "Bearer $token"]);

        $response->assertStatus(200);
    }

    public function dontruntestReports()
    {
        $user = factory('App\User')->create();
        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();

        $response = $this->actingAs($user)
             ->get('/api/reports', ['Authorization' => "Bearer $token"]);

        // echo $response->status();
        // echo $response->getContent();
        // $response = $this->get('app/logout');
        $response->assertStatus(200);
    }
}
